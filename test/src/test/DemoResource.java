package test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("demo")
public class DemoResource {
  private String m_message = "hello world!";
 
  /**
   * Will be registered at /demo 
   */
  @GET
  @Produces("text/plain")
  public String hello()  {
      return m_message;
  }
}
